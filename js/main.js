//initiate the phaser framework
var game = new Phaser.Game(640, 360, Phaser.AUTO)

//create state(s), state is where the logic of the games written
var GameState = {
    //load the assets before the game starts
    preload: function () {
        this.load.image('background', 'assets/images/background.png')
        /*
        this.load.image('chicken', 'assets/images/chicken.png')
        this.load.image('horse', 'assets/images/horse.png')
        this.load.image('pig', 'assets/images/pig.png')
        this.load.image('sheep', 'assets/images/sheep3.png')*/

        this.load.image('arrow', 'assets/images/arrow.png')

        //load spritesheet instead of images
        this.load.spritesheet('chicken', 'assets/images/chicken_spritesheet.png', 131, 200, 3);
        this.load.spritesheet('horse', 'assets/images/horse_spritesheet.png', 212, 200, 3);
        this.load.spritesheet('pig', 'assets/images/pig_spritesheet.png', 297, 200, 3);
        this.load.spritesheet('sheep', 'assets/images/sheep_spritesheet.png', 244, 200, 3);

        //audio
        this.load.audio('chickenSound', ['assets/audio/chicken.ogg', 'assets/audio/chicken.mp3'])
        this.load.audio('horseSound', ['assets/audio/horse.ogg', 'assets/audio/horse.mp3'])
        this.load.audio('pigSound', ['assets/audio/pig.ogg', 'assets/audio/pig.mp3'])
        this.load.audio('sheepSound', ['assets/audio/sheep.ogg', 'assets/audio/sheep.mp3'])
    },
    //executed after everything is loaded
    create: function () {
        //make canvas scaled to the screen size automatically + maintaning the aspect ratio
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        //align centerly
        this.scale.pageAlignHorizontally = true
        this.scale.pageAlignVertically = true
        
        //create a sprite for the background
        this.background = this.game.add.sprite(0, 0, 'background')
        // //center of the game world
        // this.chicken = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'chicken')
        // //set te anchor point of sprite
        // this.chicken.anchor.setTo(0.5, 0.5) //or just (0.5) if x=y
        // //scale sprite
        // this.chicken.scale.setTo(-1, 1)
        // //allow user input
        // this.chicken.inputEnabled = true
        // this.chicken.input.pixelPerfectClick = true
        // this.chicken.events.onInputDown.add(this.animateAnimal, this)
        
        //group for animals
        var animalData = [
            {key: 'chicken', text: 'CHICKEN', audio: 'chickenSound'},
            {key: 'horse', text: 'HORSE', audio: 'horseSound'},
            {key: 'pig', text: 'PIG', audio: 'pigSound'},
            {key: 'sheep', text: 'SHEEP', audio: 'sheepSound'}
        ]
        
        //create group
        this.animals = this.game.add.group()
        var self = this
        
        animalData.forEach(function (element) { //cannot use this in foreach
            //create each animal and save it's properties
            animal = self.animals.create(-1000, self.game.world.centerY, element.key)
            
            //saving everything that's not phase-related in an object
            animal.customParams = {text: element.text, sound: self.game.add.audio(element.audio)}
            //anchor set to center of the sprite
            animal.anchor.setTo(0.5)
            
            //create animal animation
            animal.animations.add('animate', [0, 1, 2, 1, 0, 1], 3, false)
            
            //enable input so we can touch it(animal)
            animal.inputEnabled = true
            animal.input.pixelPerfectClick = true
            animal.events.onInputDown.add(self.animateAnimal, self)
        })
        //place the first animal in the middle
        this.currentAnimal = this.animals.next() //next() -> method bawaan dari group
        this.currentAnimal.position.set(this.game.world.centerX, this.game.world.centerY)

        //show animal text
        this.showText(this.currentAnimal);
        
        //left arrow
        this.leftArrow = this.game.add.sprite(60, this.game.world.centerY, 'arrow')
        this.leftArrow.anchor.setTo(0.5)
        this.leftArrow.scale.x = -1
        this.leftArrow.customParams = {direction: -1}
        //allow user input
        this.leftArrow.inputEnabled = true
        this.leftArrow.input.pixelPerfectClick = true
        this.leftArrow.events.onInputDown.add(this.switchAnimal, this)
        
        //right arrow
        this.rightArrow = this.game.add.sprite(580, this.game.world.centerY, 'arrow')
        this.rightArrow.anchor.setTo(0.5)
        this.rightArrow.customParams = {direction: 1} //customParams avoid any crash
        //allow user input
        this.rightArrow.inputEnabled = true
        this.rightArrow.input.pixelPerfectClick = true
        this.rightArrow.events.onInputDown.add(this.switchAnimal, this)
    },
    //executed multiple time per-second
    update: function () {
        //this.sheep.angle += 30
    },
    //new own methods
    switchAnimal: function (sprite, event) {
        //saat animal masih gerak jgn lakukan apapun
        if (this.isMoving){
            return false
        }
        this.isMoving = true

        //hide text
        this.animalText.visible = false
        
        //1. get the direction of the arrow
        //2. get next animal
        //3. get final destination of current animal
        //4. move current animal to final destination
        if(sprite.customParams.direction > 0) {
            newAnimal = this.animals.next()
            newAnimal.x = -newAnimal.width/2
            endX = 640 + this.currentAnimal.width/2
        }else {
            newAnimal = this.animals.previous()
            newAnimal.x = 640 + newAnimal.width/2
            endX = -this.currentAnimal.width/2
        }
        // this.currentAnimal.x = endX
        // newAnimal.x = this.game.world.centerX
        
        //tween animation
        var newAnimalMovement = this.game.add.tween(newAnimal)
        newAnimalMovement.to({x: this.game.world.centerX}, 1000) //how the newAnimal move
        //blur
        newAnimalMovement.onComplete.add(function () {
            this.isMoving = false
            this.showText(newAnimal)
        }, this)
        newAnimalMovement.start()
        var currentAnimalMovement = this.game.add.tween(this.currentAnimal)
        currentAnimalMovement.to({x: endX}, 1000)
        currentAnimalMovement.start()
        
        this.currentAnimal = newAnimal //new current animal = newAnimal
        
        //5. set the next animal as the new current animal
        
        
    },
    showText: function(animal) {
        if(!this.animalText){
            var style = {
                font: 'bold 30pt Arial',
                fill: '#D0171B',
                align: 'center'
            }
            this.animalText = this.game.add.text(this.game.width/2, this.game.height * 0.85, '', style)
            this.animalText.anchor.setTo(0.5)
        }
        this.animalText.setText(animal.customParams.text)
        this.animalText.visible = true
        
    },
    animateAnimal: function (sprite, event) {
        //console.log('animate animal')
        sprite.play('animate')
        sprite.customParams.sound.play()
    }
}

//add game state to the game
//start the state
game.state.add('GameState', GameState)
game.state.start('GameState')